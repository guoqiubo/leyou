package com.java.cart.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.java.auth.entity.UserInfo;
import com.java.cart.entity.Cart;
import com.java.cart.feign.GoodsApiFeign;
import com.java.cart.interceptor.LoginInterceptor;
import com.java.item.entity.Sku;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author jl
 * @description
 * @date 2019-11-07 15:23
 */
@Service
public class CartService {
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    private GoodsApiFeign goodsApiFeign;

    private static final String USER_CART = "user:cart:";

    public void addCart(Cart cart) {
        Integer num = cart.getNum();
        //获取用户信息
        UserInfo userInfo = LoginInterceptor.getUserInfo();
        //查询购物车记录    <userId, skuId,  carts>
        BoundHashOperations<String, Object, Object> hashOps = redisTemplate.boundHashOps(USER_CART + userInfo.getId());
        //判断当前商品是否在购物车中
        String skuId = cart.getSkuId().toString();
        if (hashOps.hasKey(skuId)) {
            //在,更新数量
            String cartJson = hashOps.get(skuId).toString();
            cart = JSONUtil.toBean(cartJson, Cart.class);
            cart.setNum(cart.getNum() + num);
        } else {
            //不在,新增购物车记录
            Sku sku = goodsApiFeign.querySkuBySkuId(cart.getSkuId());
            cart.setUserId(userInfo.getId());
            cart.setTitle(sku.getTitle());
            cart.setImage(StrUtil.isEmpty(sku.getImages()) ? "" : sku.getImages().split(",")[0]);
            cart.setPrice(sku.getPrice());
            cart.setOwnSpec(sku.getOwnSpec());
        }
        //更新redis中的购物车
        hashOps.put(skuId, JSONUtil.parse(cart).toString());
    }

    public List<Cart> queryCarts() {
        //获取用户信息
        UserInfo userInfo = LoginInterceptor.getUserInfo();
        //判断用户是否有购物车记录
        if (!redisTemplate.hasKey(USER_CART + userInfo.getId())) {
            return null;
        }

        //查询购物车记录    <userId, skuId,  carts>
        BoundHashOperations<String, Object, Object> hashOps = redisTemplate.boundHashOps(USER_CART + userInfo.getId());
        //获取购物车Map中所有Cart值的集合
        List<Object> cartsJson = hashOps.values();
        if (CollUtil.isEmpty(cartsJson)) {
            return null;
        }
        //List<Object> ==> List<Cart>
        return cartsJson.stream().map(cartJson -> JSONUtil.toBean(cartJson.toString(), Cart.class)).collect(Collectors.toList());
    }

    public void updateNum(Cart cart) {
        Integer num = cart.getNum();
        //获取用户信息
        UserInfo userInfo = LoginInterceptor.getUserInfo();
        //判断用户是否有购物车记录
        if (!redisTemplate.hasKey(USER_CART + userInfo.getId())) {
            return;
        }
        //查询购物车记录    <userId, skuId,  carts>
        BoundHashOperations<String, Object, Object> hashOps = redisTemplate.boundHashOps(USER_CART + userInfo.getId());
        String skuId = cart.getSkuId().toString();
        String cartJson = hashOps.get(skuId).toString();
        //cartJson ==> cart
        cart = JSONUtil.toBean(cartJson, Cart.class);
        cart.setNum(num);
        //更新redis中的购物车
        hashOps.put(skuId, JSONUtil.parse(cart).toString());
    }

    public void deleteCart(String skuId) {
        //获取用户信息
        UserInfo userInfo = LoginInterceptor.getUserInfo();

        BoundHashOperations<String, Object, Object> hashOps = redisTemplate.boundHashOps(USER_CART + userInfo.getId());
        hashOps.delete(skuId);
    }
}
