package com.java.item.service;

import com.java.common.dato.CartDTO;
import com.java.common.utils.PageResult;
import com.java.item.dto.SpuDTO;
import com.java.item.entity.Sku;
import com.java.item.entity.Spu;
import com.java.item.entity.SpuDetail;

import java.util.List;

/**
 * @author jl
 * @description
 * @date 2019-10-16 16:39
 */
public interface GoodsService {

    PageResult<SpuDTO> querySpuByPage(String key, Boolean saleable, Integer page, Integer rows);

    void saveGoods(SpuDTO spuDTO);

    void updateGoods(SpuDTO spuDTO);

    SpuDetail queryDetailBySpuId(Long spuId);

    List<Sku> querySkusBySpuId(Long spuId);

    Spu querySpuById(Long id);

    Sku querySkuBySkuId(Long skuId);

    void decreaseStock(List<CartDTO> carts);
}
