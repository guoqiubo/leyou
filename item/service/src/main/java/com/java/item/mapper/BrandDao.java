package com.java.item.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.java.item.entity.Brand;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 品牌表，一个品牌下有多个商品（spu），一对多关系
 * 
 * @author jiangli
 * @email 31346337@qq.com
 * @date 2019-10-11 11:44:02
 */
@Mapper
public interface BrandDao extends BaseMapper<Brand> {

    /**
     * 根据分类id查询品牌列表
     */
    @Select("SELECT * FROM `tb_brand` tb INNER JOIN `tb_category_brand` tcb ON tb.`id`=tcb.`brand_id` WHERE tcb.`category_id` = #{cid}")
    List<Brand> queryByCid(Long cid);
}
