package com.java.item.controller;

import cn.hutool.core.collection.CollUtil;
import com.java.common.utils.PageResult;
import com.java.common.utils.QueryParams;
import com.java.item.entity.Brand;
import com.java.item.service.BrandService;
import com.java.item.service.CategoryBrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 品牌表，一个品牌下有多个商品（spu），一对多关系
 *
 * @author jiangli
 * @email 31346337@qq.com
 * @date 2019-10-11 11:44:02
 */
@RestController
@RequestMapping("/brand")
public class BrandController {
    @Autowired
    private BrandService brandService;
    @Autowired
    private CategoryBrandService categoryBrandService;

    /**
     * 分页查询品牌列表
     */
    @GetMapping("/page")
    public ResponseEntity<PageResult<Brand>> queryBrandsByPage(QueryParams queryParams) {
        PageResult<Brand> result = brandService.queryBrandsByPage(queryParams);
        if (result == null || CollUtil.isEmpty(result.getItems())) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(result);
    }

    /**
     * 新增品牌
     */
    @PostMapping
    public ResponseEntity<Void> saveBrand(Brand brand, @RequestParam List<Long> cids) {
        brandService.saveBrand(brand, cids);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    /**
     * 根据分类id查询品牌列表
     */
    @GetMapping("/cid/{cid}")
    public ResponseEntity<List<Brand>> queryByCid(@PathVariable("cid") Long cid) {
        List<Brand> brands = brandService.queryByCid(cid);
        return ResponseEntity.ok(brands);
    }

    /**
     * 修改品牌
     */
    @PutMapping
    public ResponseEntity<Void> updateBrand(Brand brand, @RequestParam List<Long> cids) {
        brandService.updateBrand(brand, cids);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    /**
     * 删除品牌
     */
    @DeleteMapping("/bid/{bid}")
    public ResponseEntity<Void> deleteBrand(@PathVariable("bid") Long bid) {
        brandService.deleteBrand(bid);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    /**
     * 根据id查询品牌
     */
    @GetMapping("{id}")
    public ResponseEntity<Brand> queryBrandById(@PathVariable("id") Long id) {
        Brand brand = brandService.getById(id);
        return brand == null ? ResponseEntity.notFound().build() : ResponseEntity.ok(brand);
    }


}
