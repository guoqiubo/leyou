package com.java.item.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.java.item.entity.SpecGroup;
import org.apache.ibatis.annotations.Mapper;

/**
 * 规格参数的分组表，每个商品分类下有多个规格参数组
 * 
 * @author jiangli
 * @email 31346337@qq.com
 * @date 2019-10-16 11:30:52
 */
@Mapper
public interface SpecGroupDao extends BaseMapper<SpecGroup> {
	
}
