package com.java.item.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.java.item.entity.Sku;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * sku表,该表表示具体的商品实体,如黑色的 64g的iphone 8
 * 
 * @author jiangli
 * @email 31346337@qq.com
 * @date 2019-10-16 11:30:52
 */
@Mapper
public interface SkuDao extends BaseMapper<Sku> {

    @Select("SELECT a.*,b.`stock` FROM `tb_sku` a LEFT JOIN `tb_stock` b ON a.`id`=b.`sku_id` WHERE a.`spu_id` = #{spuId}")
    List<Sku> querySkusBySpuId(Long spuId);
	
}
