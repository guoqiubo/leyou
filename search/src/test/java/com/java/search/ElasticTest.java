package com.java.search;

import com.java.common.utils.PageResult;
import com.java.item.dto.SpuDTO;
import com.java.search.feign.GoodsApiFeign;
import com.java.search.pojo.Goods;
import com.java.search.repository.GoodsRepository;
import com.java.search.service.SearchService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author jl
 * @description
 * @date 2019-10-28 10:24
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ElasticTest {
    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;
    @Autowired
    private GoodsRepository goodsRepository;
    @Autowired
    private SearchService searchService;
    @Autowired
    private GoodsApiFeign goodsApiFeign;

    @Test
    public void importDataToEs() {
        //创建索引
        elasticsearchTemplate.createIndex(Goods.class);
        //创建映射
        elasticsearchTemplate.putMapping(Goods.class);

        int page = 1;
        int rows = 100;

        do {
            //分页查询全部上架的spu
            PageResult<SpuDTO> spus = goodsApiFeign.querySpuByPage(null, true, page, rows);
            if (spus.getItems().size() == 0) {
                break;
            }
            //spu==>goods
            List<Goods> goodsList = spus.getItems().stream().map(e -> {
                Goods goods = new Goods();
                try {
                    goods = searchService.buildGoods(e);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
                return goods;
            }).collect(Collectors.toList());

            //保存到elasticsearch
            goodsRepository.saveAll(goodsList);

            page++;
            rows = spus.getItems().size();
        } while (rows == 100);


    }


}
