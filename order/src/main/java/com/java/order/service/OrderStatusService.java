package com.java.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.java.order.entity.OrderStatus;

/**
 * 订单状态表
 *
 * @author jiangli
 * @email 31346337@qq.com
 * @date 2019-11-08 14:20:23
 */
public interface OrderStatusService extends IService<OrderStatus> {

}

