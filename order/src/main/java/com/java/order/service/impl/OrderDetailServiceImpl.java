package com.java.order.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.order.dao.OrderDetailDao;
import com.java.order.entity.OrderDetail;
import com.java.order.service.OrderDetailService;
import org.springframework.stereotype.Service;


@Service("orderDetailService")
public class OrderDetailServiceImpl extends ServiceImpl<OrderDetailDao, OrderDetail> implements OrderDetailService {

}