package com.java.auth.feign;

import com.java.user.api.UserApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author jl
 * @description
 * @date 2019-11-05 16:01
 */
@FeignClient("user-service")
public interface UserApiFeign extends UserApi {
}
