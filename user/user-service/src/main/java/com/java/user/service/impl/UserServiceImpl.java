package com.java.user.service.impl;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.user.entity.User;
import com.java.user.mapper.UserDao;
import com.java.user.service.UserService;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserDao, User> implements UserService {
    @Autowired
    private StringRedisTemplate redisTemplate;
//    @Autowired
//    private RabbitTemplate rabbitTemplate;
    @Autowired //AmqpTemplate默认的实现就是RabbitTemplate
    private AmqpTemplate amqpTemplate;

    private static final String USER_REGISTRY = "user:registry:";

    /**
     * 校验用户名是否可用
     */
    @Override
    public Boolean checkUser(String data, Integer type) {
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        if (type.equals(1)) {
            wrapper.eq(User::getUsername,data);
        } else if (type.equals(2)) {
            wrapper.eq(User::getPhone,data);
        } else {
            return null;
        }

        return this.baseMapper.selectCount(wrapper) == 0;
    }

    /**
     * 发送注册短信
     */
    @Override
    public void sendSms(String phone) {
        if (StrUtil.isEmpty(phone)) {
            return;
        }
        //生成验证码
        String smsCode = RandomStringUtils.randomNumeric(6);
        //存入缓存
        redisTemplate.opsForValue().set(USER_REGISTRY + phone, smsCode, 30, TimeUnit.MINUTES);
        //发送消息到mq
        Map<String, String> map = new HashMap<>();
        map.put("phone", phone);
        map.put("smsCode", smsCode);
        //交换器用yml中配置的,amqpTemplate.convertAndSend("LEYOU.USER.EXCHANGE","sms", map);
        amqpTemplate.convertAndSend("user.registry", map);
        //输出短信验证码方便测试
        System.out.println(smsCode);
    }

    @Override
    public void register(String username, String password, String phone, String code) {
        //校验验证码
        String redisCode = redisTemplate.opsForValue().get(USER_REGISTRY + phone);
        if (StrUtil.isEmpty(redisCode)) {
            return;
        }

        //密码加密
        String salt = RandomUtil.randomString(20);
        String newPassword = SecureUtil.md5(password + salt);
        //新增
        User user = new User();
        user.setUsername(username);
        user.setPassword(newPassword);
        user.setPhone(phone);
        user.setSalt(salt);
        user.setCreated(new Date());
        this.baseMapper.insert(user);

        //删除redisCode
        try {
            redisTemplate.delete(USER_REGISTRY + phone);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public User queryBy(String username, String password) {
        //根据用户名查询
        User user = this.baseMapper.selectOne(new LambdaQueryWrapper<User>().eq(User::getUsername, username));
        if (user == null) {
            return null;
        }
        //查出盐,加密,比较
        String salt = user.getSalt();
        String newPassword = SecureUtil.md5(password + salt);
        if (newPassword.equals(user.getPassword())) {
            return user;
        }
        return null;
    }


}