package com.java.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.java.user.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户表
 * 
 * @author jiangli
 * @email 31346337@qq.com
 * @date 2019-11-04 16:45:42
 */
@Mapper
public interface UserDao extends BaseMapper<User> {
	
}
