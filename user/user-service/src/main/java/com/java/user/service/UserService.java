package com.java.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.java.user.entity.User;

/**
 * 用户表
 *
 * @author jiangli
 * @email 31346337@qq.com
 * @date 2019-11-04 16:45:42
 */
public interface UserService extends IService<User> {

    /**
     * 校验数据是否可用
     */
    Boolean checkUser(String data, Integer type);

    /**
     * 发送注册验证码
     */
    void sendSms(String phone);

    void register(String username, String password, String phone, String code);

    User queryBy(String username, String password);
}

