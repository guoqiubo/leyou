package com.java.upload.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author jl
 * @description
 * @date 2019-10-15 11:37
 */
public interface UploadService {
    String uploadImage(MultipartFile file);
}
