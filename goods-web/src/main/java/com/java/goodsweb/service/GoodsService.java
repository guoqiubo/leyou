package com.java.goodsweb.service;

import java.util.Map;

/**
 * @author jl
 * @description
 * @date 2019-10-31 11:52
 */
public interface GoodsService {

    Map<String, Object> loadData(Long spuId);
}
